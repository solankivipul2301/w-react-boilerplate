import React, {Component} from 'react';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import {connect} from 'react-redux';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import CreateEvent from "../Schedule/CreateEvent";
import {Button} from "reactstrap";
import _ from 'lodash';
import {openScheduleModal} from "../../containers/Full/action";

BigCalendar.setLocalizer(
  BigCalendar.momentLocalizer(moment)
);

const currDate = new Date();
const currYear = currDate.getFullYear();
const currMonth = currDate.getMonth();

class Schedule extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      editData: null,
      initData: null
    }
  }
  handleCreateEvent = () => {
    this.props.openScheduleModal(!this.state.isOpen)
    this.setState((state) => ({isOpen: true, editData: null, initData: null}));
  }
  handleClose = (isUpdate = false) => {
    if (isUpdate){
      this.props.getAllEvent()
    }
    this.setState((state) => ({isOpen: false, editData: null, initData: null}));
  }
  handleSelect = ({start, end}) => {
    this.setState((state) => ({isOpen: !state.isOpen, initData: {start: start, end: end}}));
  }

  handleEvent = (e) => {
    const {events} = this.props;
    const editData = _.find(events, function (event) {
      return e.id === event.id;
    });
    this.setState((state) => ({isOpen: !state.isOpen, editData: editData}));
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ events: nextProps.events, isOpen:nextProps.scheduleModel})
  }
  render() {
    const {userData, inviteId} = this.props
    const {events} = this.state
    return (
      <div>
        <div>
          {/*<Button color="primary" onClick={this.handleCreateEvent}>Create Event</Button>*/}
          <CreateEvent isOpen={this.state.isOpen} event={events}
                       handleClose={this.handleClose}
                       editData={this.state.editData}
                       initData={this.state.initData}
                       email={userData.email}
                       clientId={inviteId}
          />
        </div>
      <div className='schedule'>
        <BigCalendar
          events={events || []}
          views={['month', 'week', 'day']}
          step={30}
          defaultDate={new Date(currYear, currMonth, 1)}
          defaultView='month'
          toolbar={true}
          selectable
          onSelectEvent={this.handleEvent}
          onSelectSlot={this.handleSelect}
          eventPropGetter={event =>({ className: `event-${event.status}` })}
        />
      </div>
      </div>
    )
  }
}

const mapDispatchToProps = {
  openScheduleModal: (id) => openScheduleModal(id)
};

const mapStateToProps = state => ({
  contacts: state.contacts.contacts,
  loading: state.contacts.loading,
  scheduleModel: state.event.modal
});

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);


