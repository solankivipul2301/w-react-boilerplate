import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from "lodash";
import Profile from './Profile';
import {doGetContactsByUserIdInvite} from './action';
import {
  Nav,
  NavItem, NavLink, TabContent,
  TabPane
} from "reactstrap";
import Schedule from './Schedule';
import './invite-details.css';
import classnames from "classnames";
import PageLoader from "../../components/loader";
import {doGetContactsByUserId, doGetEventTypes} from '../Schedule/action';

class InviteDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'profile',
      isOpen: false,
      contacts: [],
      userData: "",
      isPageLoaded: false,
    }
  }
  getParameterByName = (name, url) => {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }
	async componentWillMount(){
    if (this.getParameterByName("schedule")) {
      this.setState({activeTab : "schedule"})
    }
    let user = await localStorage.getItem('user');
    let business = await localStorage.getItem('selectBusiness');
    if (user) {
      user = JSON.parse(user);
      this.props.doGetContactsByUserId(user.id).then(res => {
      });
      this.props.doGetEventTypes(business).then(res => {}
      );
    }
    await this.getAllEvent();
  }

  getAllEvent = () => {
    let user = localStorage.getItem('user');
    if(user){
      user = JSON.parse(user);
      const inviteId = this.props.match.params.userId;
      this.setState({isPageLoaded: true})
      doGetContactsByUserIdInvite(user.id, inviteId).then((resp) => {
        if (resp.data && resp.data.length) {
          const data = [];
          this.setState({
						userData: resp.data[0]
          });
          resp.data[0].meetings.map(event => {
            data.push({
              title: event.desc,
              start: new Date(event.startTime),
              end: new Date(event.endTime),
              ...event
            });
          });
          this.setState({events : data, userData: resp.data[0], isPageLoaded: false})
        }
      }).catch(() => {
        this.props.history.push('/contacts');
      });
    }
  }

  toggleTabs = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }
  setUserData = (userData) => {
    this.setState({userData})
  };

  componentWillReceiveProps(nextProps) {
    this.setState({contacts: nextProps.contacts});
  }

  render() {
    const {userData, activeTab, events, isPageLoaded} = this.state;
		const {location, match} = this.props;
    if(!userData) {
      return (
        <PageLoader />
      )
    }
    return (
      <div className='invite-wrapper'>
        <div className='tab-pane-invite'>
          {userData.firstName || ''} {userData.lastName || ''}
        </div>
        <div className='invite-tab'>
          <Nav tabs>
            <NavItem className={classnames({active: activeTab === 'profile'})}>
              <NavLink
                className={classnames({active: activeTab === 'profile'})}
                onClick={() => {
                  this.toggleTabs('profile');
                }}
              >
                Profile
              </NavLink>
            </NavItem>
            <NavItem className={classnames({active: activeTab === 'schedule'})}>
              <NavLink
                className={classnames({active: activeTab === 'schedule'})}
                onClick={() => {
                  this.toggleTabs('schedule');
                }}
              >
                Schedule
              </NavLink>
            </NavItem>
          </Nav>
        </div>
        <TabContent activeTab={activeTab} className='tab-content'>
          <TabPane tabId="profile">
            <Profile setUserData={this.setUserData} location={location} match={match} userData={userData}/>
          </TabPane>
          <TabPane tabId="schedule" className='schedule-calendar'>
            {isPageLoaded && <PageLoader/> }
            <Schedule events={events} userData={userData} getAllEvent={this.getAllEvent} inviteId={this.props.match.params.userId}/>
          </TabPane>

        </TabContent>
      </div>
    )
  }
}

const mapDispatchToProps = {
  doGetContactsByUserId: (id) => doGetContactsByUserId(id),
  doGetEventTypes: (id) => doGetEventTypes(id)
};

const mapStateToProps = state => ({
  contacts: state.contacts.contacts,
  loading: state.contacts.loading
});

export default connect(mapStateToProps, mapDispatchToProps)(InviteDetails);


