import React, {Component} from 'react';
import {
  Button,
  ButtonGroup,
  Label,
  Modal,
  ModalBody,
  ModalHeader,
  Popover,
  PopoverBody,
  PopoverHeader,
  Alert, Badge
} from "reactstrap";
import {connect} from 'react-redux';
import {AvField, AvForm, AvGroup, AvInput, AvRadio, AvRadioGroup} from "availity-reactstrap-validation";
import ProfileCard from "../../components/ProfileCard";
import {createUserFromContact} from './action';
import SweetAlert from "react-bootstrap-sweetalert";

class Profile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      requested: false,
      processing: false,
      showUserConfirmation: false,
      showError: false,
      showSuccess: false,
    }
  }
  showModal = () => {
    this.setState((state) => ({isOpen: !state.isOpen}));
  };
  showUserModal = () => {
    this.setState((state) => ({showUserConfirmation: true}));
  };
  onUserConfirmation = () => {
    const { userData } = this.props;
    this.setState({
      processing: true,
      showUserConfirmation: false
    });
    if(userData && userData.id){
      createUserFromContact(userData.id).then((resp) => {
        if(resp && !resp.error){
          this.setState({
            ...this.state,
            processing: false,
            showUserConfirmation: false,
            showError: false,
            showSuccess: true,
            requested: false
          });
        }
        else {
          this.setState({
            ...this.state,
            processing: false,
            showUserConfirmation: false,
            showError: true,
            showSuccess: false,
            requested: false
          })
        }
      }).catch(err => {
        this.setState({
          ...this.state,
          processing: false,
          showUserConfirmation: false,
          showError: true,
          showSuccess: false,
          requested: false
        })
      })
    }
    else {
      this.setState({
        ...this.state,
        processing: false,
        showUserConfirmation: false,
        showError: true,
        showSuccess: false,
        requested: false
      })
    }
  };
  onCancel = () => {
    this.setState({showUserConfirmation: false, requested: false});
  };

  render() {
    const {userData} = this.props;
    const {showUserConfirmation, showSuccess, showError, processing} = this.state;
    const {bio} = userData;
    const isStaff = userData.type === "staff";
		const {location, match} = this.props;
    return (
      <div className='profile'>
        <div className='left-content'>
          <div className={"m-b-5"}>
            {userData && <ProfileCard location={location} match={match} setUserData={this.props.setUserData} showEdit data={userData} /> }
            {/*{userData.status === 'pending' && <div>*/}
              {/*<div className="profile-convert"><Badge color="success" onClick={this.showUserModal}>convert to user </Badge></div>*/}
            {/*</div> }*/}
          </div>
          <SweetAlert
            warning
            showCancel
            confirmBtnText="Yes, do it!"
            cancelBtnText="Cancel"
            confirmBtnBsStyle="danger"
            cancelBtnBsStyle="default"
            show={showUserConfirmation}
            title="Are you Sure?"
            text="You want to create user from this contact."
            onConfirm={this.onUserConfirmation}
            onCancel={this.onCancel}
          />
          <SweetAlert show={showSuccess} success title="Success" onConfirm={() => {
            this.setState({
              showSuccess: false
            })
          }}>
            User Created is successfully
          </SweetAlert>
          <SweetAlert show={showError} danger title="Error while processing" onConfirm={() => {
            this.setState({
              showError: false
            })
          }}>
            Error while processing your request.
          </SweetAlert>
          <SweetAlert customClass="swal-processing" show={processing} info title="Please wait" onConfirm={()=>{}}>
            Please wait...
          </SweetAlert>
          {isStaff ? <div className="session staff add-contact static">
            <div className='session-header'>
              About
            </div>
            <div className='recent-session text-left'>
              <AvForm model={userData} className='fontSize'>
                <AvField name="email" label="Email" className="subscriptionInput" type="text" disabled={true} />
                <AvField name="phone" label="Phone Number" className="subscriptionInput" type="text" disabled={true} />
                <AvGroup className="mb-3">
                  <Label htmlFor="inputIsValid" className='input-label'>Bio</Label>
                  <div>
                    {bio}
                  </div>
                </AvGroup>
              </AvForm>
            </div>
          </div>
          : <div className="session">
            <div className='session-header'>
              Session
            </div>
            <div className='recent-session'>
              No recent sessions
            </div>
          </div>}
        </div>
        <div className="recent-activity">
          <div className='recent-activity-header'>
            {userData.firstName && <span>{userData.firstName}'s Recent Activity</span>}
          </div>
          <div className='recent-activity-content'>
            <span>No Activity Yet</span>
            <p>This client hasn't any recent activity</p>

          </div>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = {
};

const mapStateToProps = state => ({
  contacts: state.contacts.contacts,
  loading: state.contacts.loading
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);


