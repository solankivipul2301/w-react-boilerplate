import axiosApi from "../../axiosApi";

export function storeLocationData(data) {
	return {
	  type: 'LOCATION_DATA',
	  payload:data,
	};
  }
export function doGetLocationList() {
	return dispatch => axiosApi({
		method: 'GET',
		url: `locations`
	})
	.then((resp) => {
		if (resp && resp.data) {
		  dispatch(storeLocationData(resp.data));
		  return resp.data;
		}
		return resp;
	  }).catch((error, code, status) => error && error.response && error.response.data);
}
export function doAddLocationList(data) {
	return dispatch => axiosApi({
		method: 'POST',
		url: `locations`,
		data
	})
}
export function doDeleteLocationById(id) {
	return dispatch => axiosApi({
		method: 'Delete',
		url: `locations/`+id
	})
}
export function doUpdateLocationById(id, data) {
	return dispatch => axiosApi({
		method: 'PUT',
		url: `locations/`+id,
		data
	})
}