import { combineReducers } from 'redux';
import authenticateReducer from '../containers/authenticate/reducer';
import helpersReducer from '../containers/helpers/reducer';
import homeReducer from '../containers/Home/reducer';

export default combineReducers({
    authenticate: authenticateReducer,
    helpers: helpersReducer,
    home: homeReducer,
});
