import { connect } from "react-redux";
import React, { Component } from "react";
import {
    Col, Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row,
} from 'reactstrap';

import "react-toastify/dist/ReactToastify.css";
import './home.css';
import { getBlogList } from '../helpers/actions';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
    componentDidMount() {
        this.props.onGetBlogList();
    }

  componentWillUnmount() {
  }

  render() {
    const { items, blogList } = this.props;
      console.log({blogList});

    return (
        <Row>
            {items && items.map((item, itemIndex) =>
                <Col sm={3} key={itemIndex}>
                  <Card>
                    <CardImg top width="100%" src="https://dummyimage.com/318x180/cccccc/5c5c5c" alt="Card image cap" />
                    <CardBody>
                      <CardTitle>Card title</CardTitle>
                      <CardSubtitle>Card subtitle</CardSubtitle>
                      <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                      <Button>Button</Button>
                    </CardBody>
                  </Card>
                </Col>
            )}
        </Row>
    );
  }
}

const mapDispatchToProps = dispatch => ({
    onGetBlogList: () => dispatch(getBlogList()),
});

const mapStateToProps = state => ({
    blogList: state.helpers.blogList,
    items: state.helpers.items
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);