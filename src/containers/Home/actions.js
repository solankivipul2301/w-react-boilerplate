import axiosApi from "../../axiosApi";

export function updateGetItems(resultId, type, like) {
    const url = '/'
    return axiosApi({
      method: 'PUT',
      url: url,
      data: {

      }
    })
    .then((resp) => {
      return resp
    }).catch((error) =>  {
      return error && error.response && error.response.data
    });
}
export function doGetItems(searchTerm) {
    return axiosApi({
      method: 'GET',
      url: `/items`,
    })
    .then((resp) => {
      return resp
    }).catch((error) =>  {
      return error && error.response && error.response.data
    });
}

