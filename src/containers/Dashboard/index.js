import { connect } from "react-redux";
import React, { Component } from "react";

import requireAuth from '../authenticate';
import "react-toastify/dist/ReactToastify.css";

class dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  componentWillMount() {}
  componentWillUnmount() {
  }
  render() {
    return (
      <div className="app">
       dashboard PAGE
      </div>
    );
  }
}

const mapDispatchToProps = () => {
  return {};
};
const mapStateToProps = state => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(requireAuth(dashboard));
