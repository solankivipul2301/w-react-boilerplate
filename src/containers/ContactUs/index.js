import React, { Component } from 'react';
import {
    Row, Col, FormGroup, Label, Input,
    Button
} from 'reactstrap';

export class ContactUs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contactUsForm: [],
        };
    }

    inputChangeHandler = event => {
        this.setState({
            registerForm: {
                ...this.state.contactUsForm,
                [event.target.name]: event.target.value,
            },
        });
    };

    formSubmitHandler = () => {
        console.log('register user: ', this.state.contactUsForm);
    };

    render () {
        return (
            <React.Fragment>
                <Row>
                    <Col md={6} className="m-b-20">
                        <FormGroup row>
                            <Label for="fullName" sm={2}>Full Name</Label>
                            <Col sm={10}>
                                <Input
                                    type="text"
                                    name="full_name"
                                    id="fullName"
                                    placeholder="full name"
                                    onChange={this.inputChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="email" sm={2}>Email</Label>
                            <Col sm={10}>
                                <Input
                                    type="email"
                                    name="email"
                                    id="email"
                                    placeholder="email"
                                    onChange={this.inputChangeHandler}                                />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="message" sm={2}>Message</Label>
                            <Col sm={10}>
                                <Input
                                    type="textarea"
                                    name="message"
                                    id="message"
                                    onChange={this.inputChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <Button
                            color="primary"
                            onClick={this.formSubmitHandler}
                        >
                            Send Message
                        </Button>
                    </Col>
                    <Col md={6}>
                        <address>
                            <strong>Whip Inc.</strong><br />
                            38, Bhavani Nagar,<br />
                            Nr. Matavadi Circle, L. H. Road,<br />
                            Varachha, Surat 395006<br />
                            <abbr title="Phone">P:</abbr> (957) 412-1574
                        </address>

                        <address>
                            <strong>Mail</strong><br />
                            <a href="mailto:vipulsolanki0810@gmail.com">
                                vipulsolanki0810@gmail.com
                            </a>
                        </address>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default ContactUs;