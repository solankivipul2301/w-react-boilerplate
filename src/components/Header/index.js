import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import {
    Collapse, Navbar, NavbarToggler, NavbarBrand,
    Nav,NavItem,
} from 'reactstrap';
import { isLoggedIn, logOut } from '../../containers/authenticate/actions';

export class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    };

    componentWillMount = () => {
        this.props.onIsLoggedIn();
    };

    componentDidUpdate = () => {
        this.props.onIsLoggedIn();
    };

    render () {
        const { auth, onLogOut } = this.props;
        return (
            <React.Fragment>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">
                        Wish From Heart
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            {/*<NavItem>*/}
                                {/*<Link className="nav-link" to={'/'}>*/}
                                    {/*Home*/}
                                {/*</Link>*/}
                            {/*</NavItem>*/}
                            {
                                auth &&
                                <NavItem>
                                    <Link className="nav-link" to={'/dashboard'}>
                                        Dashboard
                                    </Link>
                                </NavItem>
                            }
                            {/*<NavItem>*/}
                                {/*<Link className="nav-link" to={'/wishes'}>*/}
                                    {/*Wishes*/}
                                {/*</Link>*/}
                            {/*</NavItem>*/}
                            {/*<NavItem>*/}
                                {/*<Link className="nav-link" to={'/about-us'}>*/}
                                    {/*About Us*/}
                                {/*</Link>*/}
                            {/*</NavItem>*/}
                            {/*<NavItem>*/}
                                {/*<Link className="nav-link" to={'/contact-us'}>*/}
                                    {/*Contact Us*/}
                                {/*</Link>*/}
                            {/*</NavItem>*/}
                            {/*{*/}
                                {/*!auth &&*/}
                                    {/*<React.Fragment>*/}
                                        {/*<NavItem>*/}
                                            {/*<Link className="nav-link" to={'/register'}>*/}
                                                {/*Register*/}
                                            {/*</Link>*/}
                                        {/*</NavItem>*/}
                                        {/*<NavItem>*/}
                                            {/*<Link className="nav-link" to={'/login'}>*/}
                                                {/*Login*/}
                                            {/*</Link>*/}
                                        {/*</NavItem>*/}
                                    {/*</React.Fragment>*/}
                            {/*}*/}
                            {/*{*/}
                                {/*auth &&*/}
                                {/*<NavItem>*/}
                                    {/*<a*/}
                                        {/*className="nav-link p-cur"*/}
                                        {/*onClick={() => onLogOut()}*/}
                                    {/*>*/}
                                        {/*Logout*/}
                                    {/*</a>*/}
                                {/*</NavItem>*/}
                            {/*}*/}
                        </Nav>
                    </Collapse>
                </Navbar>
            </React.Fragment>
        );
    }
}

Header.propsType = {
    auth: PropTypes.Boolean,
    onLogOut: PropTypes.func,
    onIsLoggedIn: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
    onLogOut: () => dispatch(logOut()),
    onIsLoggedIn: () => dispatch(isLoggedIn()),
});
const mapStateToProps = state => ({
    auth: state.authenticate.auth
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);