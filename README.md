Getting Started
---------------

```sh
# clone it
git clone git@gitlab.com:solankivipul2301/react-boilerplate-with-api-call.git
cd react-boilerplate-with-api-call

# Install dependencies
npm install

# Start development live-reload server
PORT=8080 npm run dev

# Start production server:
PORT=8080 npm start
```
